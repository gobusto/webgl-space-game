WEBSITE: https://opengameart.org/content/5-space-ships
AUTHORS: Viktor Hahn (Viktor.Hahn@web.de)
LICENCE: CC-BY-SA 3.0

5 fighter class space ship models. I used them to render 2D sprites, but I guess
they can be used for 3D games as well.

The .zip file contains 5 .blend files with diffuse, specular, normal and
emission textures included, ~2k verts.
