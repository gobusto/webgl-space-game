WEBSITE: https://opengameart.org/content/astronaut-1
AUTHORS: Teh_Bucket
LICENCE: CC0

Cute little lowpoly astronaut with simple projection painted texture. Silly run
animation included. 640 tris.

3d preview: http://p3d.in/RMnbO
