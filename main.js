import Simulation from './src/Simulation.js'

window.onload = function () {
  const simulation = new Simulation('#canvas-container')
  simulation.mainLoop()
}
