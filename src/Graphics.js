import * as THREE from '../lib/three.module.js'
import { OBJLoader } from '../lib/OBJLoader.js'
import { MTLLoader } from '../lib/MTLLoader.js'
import Terrain from './Terrain.js'
import Math3D from './util/Math3D.js'

class Graphics {
  constructor (selector, width, height) {
    this.renderer = new THREE.WebGLRenderer()
    this.renderer.setSize(width, height)
    this.renderer.setPixelRatio(window.devicePixelRatio)

    const target = document.querySelector(selector)
    target.appendChild(this.renderer.domElement)

    this.camera = new THREE.PerspectiveCamera(60, width/height, 0.1, 10000)
    this.scene = new THREE.Scene()

    const ambient = new THREE.AmbientLight(0xFFFFFF)
    this.scene.add(ambient)

    this.sphereGeometry = new THREE.SphereGeometry(1, 32, 32)

    this.defaultTexture = this._loadTexture('textures/default.jpg')
    this.defaultMaterial = new THREE.MeshBasicMaterial({ map: this.defaultTexture })
    this.wireframeMaterial = new THREE.MeshBasicMaterial({ wireframe: true })

    this._loadModel('models/astronaut/', 'astronaut.obj', 'astronaut.mtl').then(
      model => this.personModel = model
    )

    this._loadModel('models/ship/', 'fighter01.obj', 'fighter01.mtl').then(
      model => this.shipModel = model
    )

    this.terrain = new Terrain(20, 20)
    // const pointsMaterial = new THREE.PointsMaterial({})
    // this.scene.add(new THREE.Points(this.terrain.geometry, pointsMaterial))
    this.scene.add(new THREE.Mesh(this.terrain.geometry, this.defaultMaterial))

    // The below is all for debugging purposes...
    const points = [new THREE.Vector3(0, 0, 0), new THREE.Vector3(0, 0, 20)]
    this.axisLine = new THREE.BufferGeometry().setFromPoints(points)
    this.upMat = new THREE.LineBasicMaterial({ color: 0xffff00 })
    this.leftMat = new THREE.LineBasicMaterial({ color: 0x00ffff })
    this.forwardMat = new THREE.LineBasicMaterial({ color: 0xff00ff })
  }

  update (universe) {
    universe.getPlanets().forEach(planet => this.drawPlanet(planet))
    universe.getActors().forEach(actor => this.drawActor(actor))
    this.updateTerrain(universe)
    this.renderer.render(this.scene, this.camera)
  }

  // FIXME: This does not currently work properly for spaceships...
  updateTerrain (universe) {
    const actor = universe.getActors().find(a => a.currentPawn)
    if (!actor || !actor.currentPlanet) { return }

    this.terrain.update((x, y) => {
      // Offset the coordinates so that they are centred around [0,0]
      const d = actor.position.map((v, i) => v - actor.currentPlanet.position[i])
      const spacing = Math3D.vectorLength(d)/actor.currentPlanet.radius * 10
      const x2 = (this.terrain.xSamples/2 - x + 0.5) * spacing
      const y2 = (y + 0.5 - this.terrain.ySamples/2) * spacing

      // NOTE: This is necessary for cases where an actor is not perpendicular!
      const localUp = Math3D.directionFrom(actor.currentPlanet, actor)
      const localLeft = actor.left
      const localForward = Math3D.crossProduct(localLeft, localUp)

      // Transform the coordinates relative to the local player orientation:
      const localXYZ = [
        actor.position[0] + x2*localLeft[0] + y2*localForward[0],
        actor.position[1] + x2*localLeft[1] + y2*localForward[1],
        actor.position[2] + x2*localLeft[2] + y2*localForward[2]
      ]

      // Convert the global XYZ position into planet-local geocoordinates:
      const geo = Math3D.geoCoordsFrom(actor.currentPlanet.position, localXYZ)

      // Convert the geocoordinates back into a local XYZ "height" position:
      const heightXYZ = Math3D.xyzFromGeoCoords(
        geo.latitude,
        geo.longitude,
        actor.currentPlanet.radius,
        actor.currentPlanet.waves
      )

      // Offset the "height" XYZ so that it is relative to the planet origin:
      return [
        actor.currentPlanet.position[0] + heightXYZ[0],
        actor.currentPlanet.position[1] + heightXYZ[1],
        actor.currentPlanet.position[2] + heightXYZ[2]
      ]
    })
  }

  drawPlanet (planet) {
    if (!planet._gfx) {
      planet._gfx = new THREE.Mesh(this.sphereGeometry, this.wireframeMaterial)
      planet._gfx.scale.set(planet.radius, planet.radius, planet.radius)
      this.scene.add(planet._gfx)
    }

    planet._gfx.position.set(
      planet.position[0], planet.position[1], planet.position[2]
    )
  }

  drawActor (actor) {
    const model = actor.kind === 'ship' ? this.shipModel : this.personModel
    if (!model) { return }

    if (!actor._gfx) {
      actor._gfx = model.clone()
      actor._gfx.scale.set(actor.radius, actor.radius, actor.radius)
      this.scene.add(actor._gfx)
    }

    actor._gfx.position.set(
      actor.position[0], actor.position[1], actor.position[2]
    )

    actor._gfx.up.set(actor.up[0], actor.up[1], actor.up[2])
    actor._gfx.lookAt(
      actor.position[0] + actor.forward[0],
      actor.position[1] + actor.forward[1],
      actor.position[2] + actor.forward[2]
    )
    actor._gfx.rotateY(actor.turn)

    // The below is just for debugging purposes:
    if (!actor._lines) {
      actor._lines = [
        new THREE.Line(this.axisLine, this.upMat),
        new THREE.Line(this.axisLine, this.leftMat),
        new THREE.Line(this.axisLine, this.forwardMat)
      ]
      actor._lines.forEach(l => this.scene.add(l))
    }

    actor._lines.forEach(l => {
      l.position.set(actor.position[0], actor.position[1], actor.position[2])
    })

    actor._lines[0].lookAt(
      actor.position[0] + actor.up[0],
      actor.position[1] + actor.up[1],
      actor.position[2] + actor.up[2]
    )
    actor._lines[1].lookAt(
      actor.position[0] + actor.left[0],
      actor.position[1] + actor.left[1],
      actor.position[2] + actor.left[2]
    )
    actor._lines[2].lookAt(
      actor.position[0] + actor.forward[0],
      actor.position[1] + actor.forward[1],
      actor.position[2] + actor.forward[2]
    )

    // Position the camera where the player is.
    if (actor.currentPawn) {
      this.camera.position.x = actor.position[0]
      this.camera.position.y = actor.position[1]
      this.camera.position.z = 300//actor.position[2] + 250
/*
      this.camera.position.set(
        actor.position[0], actor.position[1], actor.position[2]
      )

      this.camera.up.set(actor.up[0], actor.up[1], actor.up[2])
      this.camera.lookAt(
        actor.position[0] + actor.forward[0],
        actor.position[1] + actor.forward[1],
        actor.position[2] + actor.forward[2]
      )
      this.camera.rotateY(actor.turn)
      this.camera.translateZ(60)
      this.camera.translateY(8)
*/
    }
  }

  _loadTexture (url) {
    const textureLoader = new THREE.TextureLoader()
    const texture = textureLoader.load(url)
    texture.wrapS = texture.wrapT = THREE.RepeatWrapping
    return texture
  }

  _loadModel (path, obj, mtl) {
    return new Promise((resolve, reject) => {
      const loadOBJ = (materials) => {
        const loader = new OBJLoader().setPath(path)
        if (materials) { materials.preload() ; loader.setMaterials(materials) }
        loader.load(obj, model => resolve(model))
      }

      mtl ? new MTLLoader().setPath(path).load(mtl, loadOBJ) : loadOBJ(null)
    })
  }
}

export default Graphics
