import Joypad from './util/Joypad.js'

class UserInput {
  constructor () {
    this.keys = {}
    this.joypad = new Joypad()
    document.body.onkeydown = this._keyboardEvent.bind(this)
    document.body.onkeyup = this._keyboardEvent.bind(this)
  }

  getState () {
    this.joypad.update()

    const axis = [
      this.joypad.leftStickX(),
      this.joypad.leftStickY(),
      this.joypad.rightStickX(),
      this.joypad.rightStickY()
    ]

    const fire = [
      this.joypad.face1(),
      this.joypad.face2(),
      this.joypad.face3(),
      this.joypad.face4()
    ]

    let data = { axis: axis.map(v => Math.abs(v) > 0.3 ? v : 0), button: fire }

    if (this.keys['a']) { data.axis[0] = -1 }
    if (this.keys['d']) { data.axis[0] = 1 }
    if (this.keys['w']) { data.axis[1] = -1 }
    if (this.keys['s']) { data.axis[1] = 1 }

    if (this.keys['arrowleft']) { data.axis[2] = -1 }
    if (this.keys['arrowright']) { data.axis[2] = 1 }
    if (this.keys['arrowup']) { data.axis[3] = -1 }
    if (this.keys['arrowdown']) { data.axis[3] = 1 }

    if (this.keys[' ']) { data.button[0] = true }
    if (this.keys['enter']) { data.button[1] = true }

    return data
  }

  _keyboardEvent (event) {
    const down = event.type === 'keydown'
    if (!event.repeat) { this.keys[event.key.toLowerCase()] = down }
  }
}

export default UserInput
