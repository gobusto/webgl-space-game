// Calculate the cross product of two vectors.
export function crossProduct (foo, bar) {
  return [
      foo[1] * bar[2] - foo[2] * bar[1],
    -(foo[0] * bar[2] - foo[2] * bar[0]),
      foo[0] * bar[1] - foo[1] * bar[0]
  ]
}

// Get a (normalised) direction vector based on two XYZ points.
export function directionFrom (origin, point) {
  const a = point.position[0] - origin.position[0]
  const b = point.position[1] - origin.position[1]
  const c = point.position[2] - origin.position[2]
  return normalize([a, b, c])
}

// Euclidean -> Lat/Long. See https://www.oc.nps.edu/oc2902w/coord/coordcvt.pdf
export function geoCoordsFrom (origin, point) {
  const x = point[0] - origin[0]
  const y = point[1] - origin[1]
  const z = point[2] - origin[2]
  const p = Math.sqrt(x*x + y*y)

  return { longitude: Math.atan2(y, x), latitude: Math.atan2(z, p) }
}

// "Normalize" a 3D vector such that it has an overall length of 1.0
export function normalize (xyz) {
  const length = vectorLength(xyz)
  return length ? xyz.map(value => value/length) : [0, 0, 0]
}

// Determine if two spheres overlap.
export function overlap (foo, bar) {
  const a = foo.position[0] - bar.position[0]
  const b = foo.position[1] - bar.position[1]
  const c = foo.position[2] - bar.position[2]
  const d = foo.radius + bar.radius
  return a*a + b*b + c*c <= d*d
}

// Calculate the "length" of a 3D vector.
export function vectorLength (xyz) {
  return Math.sqrt(xyz[0]*xyz[0] + xyz[1]*xyz[1] + xyz[2]*xyz[2])
}

// Convert a geographic coordinate into a (sine-wave-modulated) XYZ coordinate.
export function xyzFromGeoCoords (latitude, longitude, radius, waves) {
  const sinLatitude = Math.sin(latitude)
  const cosLatitude = Math.cos(latitude)
  const sinLongitude = Math.sin(longitude)
  const cosLongitude = Math.cos(longitude)

  // Convert the list of waves into an array of specific values for this point:
  const values = waves.map(wave => {
    const value = wave.useLatitude ? latitude : longitude
    const s = cosLatitude // Set this to 1.0 for "pure" results...
    return Math.sin((value + wave.offset) * wave.frequency) * wave.amplitude * s
  })

  // Add the values (and the radius!) together to determine the "combined" XYZ:
  const xyz = [0, 0, 0]
  values.concat([radius]).forEach(value => {
    xyz[0] += cosLatitude * value * cosLongitude
    xyz[1] += cosLatitude * value * sinLongitude
    xyz[2] += sinLatitude * value
  })
  return xyz
}

export default {
  crossProduct,
  directionFrom,
  geoCoordsFrom,
  normalize,
  overlap,
  vectorLength,
  xyzFromGeoCoords
}
