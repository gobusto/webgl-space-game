import * as THREE from '../lib/three.module.js'

// Represents a simple "terrain" height grid for use with three.js
class Terrain {
  constructor (xSamples, ySamples) {
    this.xSamples = xSamples
    this.ySamples = ySamples

    let uv = []
    let verts = []
    let indices = []

    for (let y = 0; y < this.ySamples; ++y) {
      for (let x = 0; x < this.xSamples; ++x) {
        verts.push(0, 0, 0) // Not calculated yet...
        uv.push(x, y)

        if (x > 0 && y > 0) {
          indices.push(
            ((y - 1) * this.xSamples) + (x - 1),
            ((y - 1) * this.xSamples) + (x - 0),
            ((y - 0) * this.xSamples) + (x - 1),
            ((y - 0) * this.xSamples) + (x - 0),
            ((y - 0) * this.xSamples) + (x - 1),
            ((y - 1) * this.xSamples) + (x - 0)
          )
        }
      }
    }

    this.geometry = new THREE.BufferGeometry()
    this.geometry.setIndex(indices)
    this.geometry.setAttribute('uv', new THREE.Float32BufferAttribute(uv, 2))
    this.geometry.setAttribute('position', new THREE.Float32BufferAttribute(verts, 3))
  }

  update (xyzFunc) {
    for (let y = 0; y < this.ySamples; ++y) {
      for (let x = 0; x < this.xSamples; ++x) {
        const xyz = xyzFunc(x, y)
        const i = (y * this.xSamples) + x

        this.geometry.attributes.position.array[i*3 + 0] = xyz[0]
        this.geometry.attributes.position.array[i*3 + 1] = xyz[1]
        this.geometry.attributes.position.array[i*3 + 2] = xyz[2]
      }
    }
    this.geometry.attributes.position.needsUpdate = true
    this.geometry.computeBoundingSphere()
  }
}

export default Terrain
