import Math3D from './util/Math3D.js'

// TODO: These should be stored (or generated...) on a per-planet basis!
const waves = [
  { offset: 1, frequency: 3, amplitude: 0.6*5 },
  { offset: 1, frequency: 5, amplitude: 0.5*5 },
  { offset: 0, frequency: 7, amplitude: 0.2*5 },
  { offset: 2, frequency: 13, amplitude: 0.3*5, useLatitude: true },
  { offset: 2, frequency: 9, amplitude: 0.4*5, useLatitude: true },
  { offset: 0, frequency: 5, amplitude: 0.5*5, useLatitude: true }
]

class Universe {
  constructor () {
    this.planets = [
      { position: [200, 150, 0], radius: 90, atmosphere: 30, waves: waves },
      { position: [450, 300, 0], radius: 100, atmosphere: 30, waves: waves },
      { position: [750, 350, 0], radius: 80, atmosphere: 50, waves: waves }
    ]

    this.actors = [
      {
        currentPawn: true,
        kind: 'person',
        position: [200, 280, 0],
        radius: 10,
        turn: 0,
        velocity: [0, 0, 0],
        up: [1, 0, 0],
        left: [0, 1, 0],
        forward: [0, 0, 1]
      },
      {
        kind: 'ship',
        position: [300, 150, 0],
        radius: 10,
        turn: 0,
        velocity: [0, 0, 0],
        up: [1, 0, 0],
        left: [0, 1, 0],
        forward: [0, 0, 1]
      }
    ]
  }

  update (userInput) {
    this.actors.forEach(actor => {
      // Which planet are we on? May be undefined!
      actor.currentPlanet = this.planets.find(p =>
        Math3D.overlap(actor, { position: p.position, radius: p.radius+p.atmosphere })
      )

      const myInput = actor.currentPawn ? userInput : (
        { axis: [0, 0, 0, 0], button: [false, false, false, false] }
      )

      // Most actor types will "inherit" their up-ness from the nearest planet:
      const localUp = actor.currentPlanet ? Math3D.directionFrom(actor.currentPlanet, actor) : actor.up
      if (actor.currentPlanet && actor.kind !== 'ship') { actor.up = localUp }

      if (actor.kind === 'person') {
        actor.velocity[0] -= 0.3 // Gravity.
        if (myInput.button[0]) { actor.velocity[0] += 0.4 }

        actor.turn -= myInput.axis[2] * 0.05

        actor.velocity[1] += (Math.cos(actor.turn) * myInput.axis[0] * 2) + (Math.sin(actor.turn) * myInput.axis[1] * 2)
        actor.velocity[2] += (Math.sin(actor.turn) * myInput.axis[0] * 2) - (Math.cos(actor.turn) * myInput.axis[1] * 2)

        actor.velocity[1] *= 0.5
        actor.velocity[2] *= 0.5
      } else if (actor.kind === 'ship') {
        actor.velocity[2] += myInput.button[0] ? 0.5 : 0

        // Ships "turn" by manipulating the frame of reference i.e. their axes:
        const turn = myInput.axis[0] * 0.03
        const look = myInput.axis[1] * -0.03

        actor.forward = actor.forward.map(
          (value, axis) => value + actor.left[axis]*turn + actor.up[axis]*look
        )
        actor.up = Math3D.crossProduct(actor.left, actor.forward)

        actor.velocity[0] *= 0.9
        actor.velocity[1] *= 0.9
        actor.velocity[2] *= 0.9
      }

      actor.left = Math3D.normalize(Math3D.crossProduct(actor.forward, actor.up))
      actor.forward = Math3D.crossProduct(actor.up, actor.left)

      actor.position[0] += actor.velocity[0] * actor.up[0]
      actor.position[1] += actor.velocity[0] * actor.up[1]
      actor.position[2] += actor.velocity[0] * actor.up[2]

      actor.position[0] += actor.velocity[1] * actor.left[0]
      actor.position[1] += actor.velocity[1] * actor.left[1]
      actor.position[2] += actor.velocity[1] * actor.left[2]

      actor.position[0] += actor.velocity[2] * actor.forward[0]
      actor.position[1] += actor.velocity[2] * actor.forward[1]
      actor.position[2] += actor.velocity[2] * actor.forward[2]

      if (actor.currentPlanet) {
        const geo = Math3D.geoCoordsFrom(actor.currentPlanet.position, actor.position)
        const heightXYZ = Math3D.xyzFromGeoCoords(
          geo.latitude,
          geo.longitude,
          actor.currentPlanet.radius,
          actor.currentPlanet.waves
        )

        // Override the radius with the height at this exact point:
        const fakePlanet = {
          position: actor.currentPlanet.position,
          radius: Math3D.vectorLength(heightXYZ)
        }

        // Are we touching the planet surface?
        if (Math3D.overlap(actor, fakePlanet)) {
          // Position the actor EXACTLY on the surface of the planet:
          const d = actor.radius + fakePlanet.radius
          actor.position[0] = actor.currentPlanet.position[0] + d * localUp[0]
          actor.position[1] = actor.currentPlanet.position[1] + d * localUp[1]
          actor.position[2] = actor.currentPlanet.position[2] + d * localUp[2]
          actor.velocity[0] = 0.3
        }
      }
    })

    // HACK: Change the currently-controlled actor.
    if (!this.fireHeld && userInput.button[1]) {
      this.actors.forEach(actor => actor.currentPawn = !actor.currentPawn)
    }
    this.fireHeld = userInput.button[1]
  }

  getPlanets () {
    return this.planets
  }

  getActors () {
    return this.actors
  }
}

export default Universe
