"use strict"

let renderer, camera, scene

function setupScene () {
  renderer = new THREE.WebGLRenderer()
  renderer.setSize(640, 480)
  renderer.setPixelRatio(window.devicePixelRatio)
  document.body.appendChild(renderer.domElement)

  camera = new THREE.PerspectiveCamera(60, 640/480, 0.01, 100)
  camera.position.set(0, 0, 25)

  scene = new THREE.Scene()

  const directionalLight = new THREE.DirectionalLight(0xffffff, 1.0)
  scene.add(directionalLight)
  directionalLight.position.set(5, 5, 5)
}

// We could use a canvas for this, but this is easier:
function createTexture () {
  const img = document.createElement('img')
  img.src = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAIAAAACCAYAAABytg0kAAAAFklEQVQIW2P8z8DwnxFIMAJpIGBkAAA8/wb+YGYULgAAAABJRU5ErkJggg=='

  const texture = new THREE.Texture(img)
  texture.wrapS = texture.wrapT = THREE.RepeatWrapping
  texture.minFilter = texture.magFilter = THREE.NearestFilter
  texture.needsUpdate = true
  return texture
}

// This, by itself, is the basic solution:
function normalise (vec) {
  const len = Math.sqrt(vec.reduce((t, v) => (v * v) + t, 0))
  return len ? vec.map(value => value / len) : vec
}

// https://medium.com/@oscarsc/four-ways-to-create-a-mesh-for-a-sphere-d7956b825db4
// https://catlikecoding.com/unity/tutorials/cube-sphere/
function spherify (v) {
  const p = { x: v[0]*v[0], y: v[1]*v[1], z: v[2]*v[2] }
  const x = v[0] * Math.sqrt(1 - p.y/2 - p.z/2 + p.y*p.z/3)
  const y = v[1] * Math.sqrt(1 - p.x/2 - p.z/2 + p.x*p.z/3)
  const z = v[2] * Math.sqrt(1 - p.x/2 - p.y/2 + p.x*p.y/3)
  return [x, y, z]
}

// This does this OPPOSITE of what we want -- biasing things towards corners
const roundify = vec => normalise(vec.map(
  value => Math.sin(value/2 * Math.PI)
))

function createSphere (radius, slices) {
  if (!radius || radius <= 0) { radius = 1 }
  if (!slices || slices <= 1) { slices = 1 }

  let uv = []
  let verts = []
  let indices = []

  for (let side = 0; side < 6; ++side) {
    for (let y = 0; y <= slices; ++y) {
      for (let x = 0; x <= slices; ++x) {
        const sx = (x - (slices / 2)) / (slices / 2)
        const sy = (y - (slices / 2)) / (slices / 2)

        //const triad = normalise([
        const triad = spherify([
        //const triad = roundify([
          [-1, sx, -sy],
          [+1, sx,  sy],
          [sx, -1,  sy],
          [sx, +1, -sy],
          [sx, -sy, -1],
          [sx,  sy, +1]
        ][side])

        verts.push(triad[0] * radius, triad[1] * radius, triad[2] * radius)
        uv.push(x, y)

        if (x && y) {
          const thisFace = side * (slices + 1) * (slices + 1)
          const thisLine = thisFace + ((y - 1) * (slices + 1))
          const nextLine = thisFace + ((y - 0) * (slices + 1))

          indices.push(
            thisLine + (x - 1),
            thisLine + (x - 0),
            nextLine + (x - 1),
            nextLine + (x - 0),
            nextLine + (x - 1),
            thisLine + (x - 0),
          )
        }
      }
    }
  }

  const geometry = new THREE.BufferGeometry()
  geometry.setIndex(indices)
  geometry.setAttribute('position', new THREE.Float32BufferAttribute(verts, 3))
  geometry.setAttribute('uv', new THREE.Float32BufferAttribute(uv, 2))
  geometry.computeVertexNormals()
  return geometry
}

function updateDisplay (timer) {
  requestAnimationFrame(updateDisplay)

  window.planet.setRotationFromAxisAngle(new THREE.Vector3(1, 0, 0), timer/2000)

  renderer.render(scene, camera)
}

window.onload = function () {
  setupScene()

  const texture = createTexture()
  const geometry = createSphere(10, 9)
  const material = new THREE.MeshPhongMaterial({ map: texture })
  window.planet = new THREE.Mesh(geometry, material)
  scene.add(window.planet)

  updateDisplay()
}
