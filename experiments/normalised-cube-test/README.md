Normalised Cube Test
====================

This is a simple demonstration of the "normalised cube" approach for creating a
spherical mesh, implemented using ThreeJS.

References
----------

* <https://iquilezles.org/articles/patchedsphere/>
* <https://catlikecoding.com/unity/tutorials/cube-sphere/>
* <https://medium.com/@oscarsc/four-ways-to-create-a-mesh-for-a-sphere-d7956b825db4>
