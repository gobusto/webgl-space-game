class UserInput {
  constructor () {
    this.keys = {}
    document.body.onkeydown = this._keyboardEvent.bind(this)
    document.body.onkeyup = this._keyboardEvent.bind(this)
  }

  getState () {
    let result = { axis: [0, 0], fire: [false, false] }

    const joypad = navigator.getGamepads()[0]
    if (joypad) {
      if (Math.abs(joypad.axes[0]) > 0.3) { result.axis[0] = joypad.axes[0] }
      if (Math.abs(joypad.axes[1]) > 0.3) { result.axis[1] = joypad.axes[1] }
      result.fire[0] = joypad.buttons[0].pressed
      result.fire[1] = joypad.buttons[1].pressed
    }

    if (this.keys['a']) { result.axis[0] = -1 }
    if (this.keys['d']) { result.axis[0] = 1 }
    if (this.keys['w']) { result.axis[1] = -1 }
    if (this.keys['s']) { result.axis[1] = 1 }
    if (this.keys[' ']) { result.fire[0] = true }
    if (this.keys['Enter']) { result.fire[1] = true }

    return result
  }

  _keyboardEvent (event) {
    if (!event.repeat) { this.keys[event.key] = event.type === 'keydown' }
  }
}

// Determine if two circles overlap.
function overlap (foo, bar) {
  const a = foo.position[0] - bar.position[0]
  const b = foo.position[1] - bar.position[1]
  const d = foo.radius + bar.radius
  return a*a + b*b <= d*d
}

class Universe {
  constructor () {
    this.planets = [
      { position: [200, 150], radius: 90, atmosphere: 30 },
      { position: [450, 300], radius: 100, atmosphere: 30 },
      { position: [750, 350], radius: 80, atmosphere: 50 }
    ]

    this.actors = [
      { kind: 'person', position: [250, 150], radius: 10, velocity: [0, 0], angle: 0 }
    ]
  }

  update (userInput) {
    this.actors.forEach(actor => {
      // Which planet are we on? May be undefined!
      const planet = this.planets.find(p =>
        overlap(actor, { position: p.position, radius: p.radius+p.atmosphere })
      )

      // HACK: Change a person into a ship or vice versa:
      if (!this.fireHeld && userInput.fire[1] && planet) {
        actor.kind = actor.kind === 'person' ? 'ship' : 'person'
      }
      this.fireHeld = userInput.fire[1]

      // Normal user input:
      if (actor.kind === 'person') {
        // Movement and Gravity (relative to local planet "up" direction):
        actor.velocity[0] -= 0.3
        if (userInput.fire[0]) { actor.velocity[0] += 0.4 }
        actor.velocity[1] += userInput.axis[0] * 2
        actor.velocity[1] *= 0.5

        // If we're on a planet, update the "up" angle for the actor:
        if (planet) {
          const a = actor.position[0] - planet.position[0]
          const b = actor.position[1] - planet.position[1]
          actor.angle = Math.atan2(b, a)
        }
      } else if (actor.kind === 'ship') {
        // Movement is not relative to anything; use global coordinates.
        actor.angle += userInput.axis[0] * 0.03
        if (userInput.fire[0]) { actor.velocity[0] += 0.7 }
        actor.velocity[0] *= 0.9
        actor.velocity[1] *= 0.9
      }

      actor.position[0] += actor.velocity[0] * Math.cos(actor.angle)
      actor.position[0] -= actor.velocity[1] * Math.sin(actor.angle)
      actor.position[1] += actor.velocity[0] * Math.sin(actor.angle)
      actor.position[1] += actor.velocity[1] * Math.cos(actor.angle)

      // Are we touching the planet surface?
      if (planet && overlap(actor, planet)) {
        const a = actor.position[0] - planet.position[0]
        const b = actor.position[1] - planet.position[1]
        const planetAngle = Math.atan2(b, a)
        if (actor.kind === 'person') { actor.angle = planetAngle }

        // Position the actor EXACTLY on the surface of the planet:
        const d = actor.radius + planet.radius
        actor.position[0] = planet.position[0] + d * Math.cos(planetAngle)
        actor.position[1] = planet.position[1] + d * Math.sin(planetAngle)
        actor.velocity[0] = 0
      }
    })
  }

  getPlanets () {
    return this.planets
  }

  getActors () {
    return this.actors
  }
}

class Graphics {
  constructor (selector) {
    this.canvas = document.querySelector(selector)
    this.ctx = this.canvas.getContext('2d')

    this.images = {
      person: document.querySelector('[alt="person"]'),
      ship: document.querySelector('[alt="ship"]')
    }
  }

  update (universe) {
    this.ctx.fillStyle = '#000000'
    this.ctx.fillRect(0, 0, this.canvas.width, this.canvas.height)

    universe.getPlanets().forEach(planet => {
      const radius = planet.radius + planet.atmosphere
      this.ctx.fillStyle = '#555555'
      this.drawCircle(planet.position[0], planet.position[1], radius)
    })

    universe.getPlanets().forEach(planet => {
      this.ctx.fillStyle = '#0088FF'
      this.drawCircle(planet.position[0], planet.position[1], planet.radius)
    })

    universe.getActors().forEach(actor => {
      this.ctx.save()
      this.ctx.translate(actor.position[0], actor.position[1])
      this.ctx.rotate(actor.angle)
      this.drawImage(this.images[actor.kind], actor.radius)
      this.ctx.restore()
    })
  }

  drawCircle (x, y, r) {
    this.ctx.beginPath()
    this.ctx.ellipse(x, y, r, r, 0, 0, 2 * Math.PI)
    this.ctx.fill()
  }

  drawImage (image, radius) {
    if (!image || !image.naturalWidth) { return }
    this.ctx.drawImage(image, -radius, -radius, radius * 2, radius * 2)
  }
}

class Simulation {
  constructor () {
    this.userInput = new UserInput()
    this.universe = new Universe()
    this.graphics = new Graphics('canvas')
  }

  mainLoop (currentTime) {
    requestAnimationFrame(this.mainLoop.bind(this))
    if (this.lastUpdate && this.lastUpdate > currentTime - 16) { return }
    this.lastUpdate = currentTime

    this.universe.update(this.userInput.getState())
    this.graphics.update(this.universe)
  }
}


window.onload = function () {
  const simulation = new Simulation()
  simulation.mainLoop()
}
