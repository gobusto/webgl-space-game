"use strict"

// We need 6 mesh buffers (and instances) -- one for each face of the cube:
let cubeFaceGeometry = [null, null, null, null, null, null]
let cubeFaceInstance = [null, null, null, null, null, null]

// These methods are used to determine when any particular side is "nearby":
const drawNegative = (xyz, axis) => xyz[axis] < -0.57
const drawPositive = (xyz, axis) => xyz[axis] > +0.57

// Using this is the simplest approach for morphing a cube into a sphere...
// function normalise (vec) {
//   const len = Math.sqrt(vec.reduce((t, v) => (v * v) + t, 0))
//   return len ? vec.map(value => value / len) : vec
// }

// ...but this approach results in more evenly-spaced vertices:
function spherify (vec) {
  const p = { x: vec[0]*vec[0], y: vec[1]*vec[1], z: vec[2]*vec[2] }
  const x = vec[0] * Math.sqrt(1 - p.y/2 - p.z/2 + p.y*p.z/3)
  const y = vec[1] * Math.sqrt(1 - p.x/2 - p.z/2 + p.x*p.z/3)
  const z = vec[2] * Math.sqrt(1 - p.x/2 - p.y/2 + p.x*p.y/3)
  return [x, y, z]
}

// Create a geometry object with enough space for an X*Y grid of triangles:
function createMeshBuffer (xPoints, yPoints) {
  if (!xPoints || xPoints < 2) { xPoints = 2 }
  if (!yPoints || yPoints < 2) { yPoints = 2 }

  let uv = []
  let verts = []
  let indices = []

  for (let y = 0; y < yPoints; ++y) {
    for (let x = 0; x < xPoints; ++x) {
      verts.push(0, 0, 0) // These will be overwritten, so can be anything...
      uv.push(x, y)

      if (x && y) {
        indices.push(
          // Triangle 1:
          ((y - 1) * xPoints) + (x - 1),
          ((y - 1) * xPoints) + (x - 0),
          ((y - 0) * xPoints) + (x - 1),
          // Triangle 2:
          ((y - 0) * xPoints) + (x - 0),
          ((y - 0) * xPoints) + (x - 1),
          ((y - 1) * xPoints) + (x - 0)
        )
      }
    }
  }

  const geometry = new THREE.BufferGeometry()
  geometry.setIndex(indices)
  geometry.setAttribute('position', new THREE.Float32BufferAttribute(verts, 3))
  geometry.setAttribute('uv', new THREE.Float32BufferAttribute(uv, 2))
  geometry.userData.xPoints = xPoints
  geometry.userData.yPoints = yPoints
  return geometry
}

// This is where we actually define the vertex positions for a single "face":
function updateMeshBuffer (geometry, params) {
  const radius = params.radius || 1

  // We can't draw fewer slices than asked to, so enforce a minimum:
  const xSlices = Math.max(params.xSlices || 0, geometry.userData.xPoints - 1)
  const ySlices = Math.max(params.ySlices || 0, geometry.userData.yPoints - 1)

  // We also can't offset our "window" out of bounds, so:
  const xOffset = Math.min(
    Math.max(params.xOffset || 0, 0), xSlices - (geometry.userData.xPoints - 1)
  )
  const yOffset = Math.min(
    Math.max(params.yOffset || 0, 0), ySlices - (geometry.userData.yPoints - 1)
  )

  for (let y = 0; y < geometry.userData.yPoints; ++y) {
    for (let x = 0; x < geometry.userData.xPoints; ++x) {
      const sx = ((x + xOffset) - (xSlices / 2)) / (xSlices / 2)
      const sy = ((y + yOffset) - (ySlices / 2)) / (ySlices / 2)

      const xyz = spherify([
        [-1, sx, -sy],
        [+1, sx,  sy],
        [sx, -1,  sy],
        [sx, +1, -sy],
        [sx, -sy, -1],
        [sx,  sy, +1]
      ][params.side || 0])

      const i = (y * geometry.userData.xPoints) + x
      geometry.attributes.position.array[i*3 + 0] = xyz[0] * radius
      geometry.attributes.position.array[i*3 + 1] = xyz[1] * radius
      geometry.attributes.position.array[i*3 + 2] = xyz[2] * radius
    }
  }

  geometry.attributes.position.needsUpdate = true
  geometry.computeVertexNormals()
  geometry.computeBoundingSphere()
}

// This calculates what the offsets should be for a "window" into a side:
function calculateOffsets (xyz, params) {
  // NOTE: XYZ is assumed to be a unit-length vector, so no `normalise()` here!
  xyz = xyz.map(v => v*2) // Good enough(?) to fix the corners.

  // How many "invisible" extra tiles are there in each direction?
  const spareX = params.xTotal - params.xShown
  const spareY = params.yTotal - params.yShown

  // This is based off the coordinate we spherify in `updateMeshBuffer()`...
  const xIndex = params.side < 2 ? 1 : 0
  const yIndex = params.side < 4 ? 2 : 1
  const invert = [1, 2, 5].includes(params.side) ? 1 : -1
  // ...we could write THIS out as a pair of arrays too, but this is shorter:
  const xOffset = ((xyz[xIndex]         )+1)/2 * spareX
  const yOffset = ((xyz[yIndex] * invert)+1)/2 * spareY

  return {
    side: params.side,
    xSlices: params.xTotal,
    ySlices: params.yTotal,
    xOffset: Math.round(xOffset),
    yOffset: Math.round(yOffset),
    radius: params.radius,
  }
}

// The rest of this file is essentially just "normal ThreeJS drawing stuff":
let renderer, camera, scene, marker

function setupScene () {
  renderer = new THREE.WebGLRenderer()
  renderer.setSize(640, 480)
  renderer.setPixelRatio(window.devicePixelRatio)
  document.body.appendChild(renderer.domElement)

  camera = new THREE.PerspectiveCamera(60, 640/480, 0.01, 100)
  camera.position.set(0, 0, 25)

  scene = new THREE.Scene()

  const ambient = new THREE.AmbientLight(0x666666)
  scene.add(ambient)

  const directionalLight = new THREE.DirectionalLight(0xffffff, 1.0)
  scene.add(directionalLight)
  directionalLight.position.set(5, 5, 5)
}

function createTexture () {
  const img = document.createElement('img')
  img.src = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAIAAAACCAYAAABytg0kAAAAFklEQVQIW2P8z8DwnxFIMAJpIGBkAAA8/wb+YGYULgAAAABJRU5ErkJggg=='

  const texture = new THREE.Texture(img)
  texture.wrapS = texture.wrapT = THREE.RepeatWrapping
  texture.minFilter = texture.magFilter = THREE.NearestFilter
  texture.needsUpdate = true
  return texture
}

function updateDisplay (timer) {
  requestAnimationFrame(updateDisplay)
  if (!timer) { return }

  const radius = 100

  // Pick a (unit length!) position vector:
  const lat = timer / 20000
  const lng = timer / 15000
  const xyz = [
    Math.sin(lat) * Math.cos(lng),
    Math.cos(lat),
    Math.sin(lat) * Math.sin(lng)
  ]

  const offset = radius + 0.5
  marker.position.set(xyz[0] * offset, xyz[1] * offset, xyz[2] * offset)

  // Position the camera:
  const distance = radius + 40
  camera.position.set(xyz[0] * distance, xyz[1] * distance, xyz[2] * distance)
  camera.lookAt(0, 0, 0)

  // Draw the relevant side(s):
  cubeFaceGeometry.forEach((geometry, side) => {
    const axis = Math.floor(side/2)
    const draw = side % 2 ? drawPositive(xyz, axis) : drawNegative(xyz, axis)

    // Hide faces which we don't currrently need to draw:
    cubeFaceInstance[side].visible = draw
    if (!draw) { return }

    // Update any faces which we DO need to draw:
    updateMeshBuffer(geometry, calculateOffsets(xyz, {
      side: side,
      xTotal: 20,
      yTotal: 20,
      xShown: geometry.userData.xPoints - 1,
      yShown: geometry.userData.yPoints - 1,
      radius: radius,
    }))
  })

  renderer.render(scene, camera)
}

window.onload = function () {
  setupScene()

  const texture = createTexture()
  const material = new THREE.MeshPhongMaterial({ map: texture })

  // Allocate a buffer (and instance) for each side of the cube:
  cubeFaceGeometry.forEach((_, side) => {
    cubeFaceGeometry[side] = createMeshBuffer(7, 7)
    cubeFaceInstance[side] = new THREE.Mesh(cubeFaceGeometry[side], material)
    scene.add(cubeFaceInstance[side])
  })

  // We also want a "marker" object to see where we're "standing", so:
  const markerGeometry = new THREE.SphereGeometry()
  const markerMaterial = new THREE.MeshBasicMaterial()
  marker = new THREE.Mesh(markerGeometry, markerMaterial)
  scene.add(marker)

  updateDisplay()
}
