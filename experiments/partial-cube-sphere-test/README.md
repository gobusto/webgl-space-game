Partial Cube Sphere Test-
========================

An attempt to render a partial surface of a normalised cube via ThreeJS.

The idea would be for a person standing on the surface of a planet to see local
terrain, whilst anything outside of their immediate view is not drawn.

Implementation options
----------------------

Two possible approaches for this problem are explained below:

* Option 1: Use a single "tile-grid" geometry object. Share its vertex/uv/index
  arrays across 1-3 sides of the cube when an edge or corner is "nearby".
* Option 2: Have 1-3 separate tile-grid geometries based on the number of sides
  we're currently trying to render.

A third option would be to use an "index-less" mesh -- that is, only allocating
arrays for vertices and texture coordinates -- but this would need more memory,
and would prevent automatic per-vertex normal generation entirely.

The "single geometry" approach
------------------------------

Option 1 is potentially more memory-efficient near sides/corners, but also more
complex to implement correctly; allocating more vertex (and texture coordinate)
space than is absolutely necessary might simplify the splitting of array space,
at the cost of being a less-perfect solution.

Option 1 could potentially use auto-generated vertex normals if common vertices
were shared across "faces" of the cube, rather than simply duplicated. This may
further complicate a "correct" implementation, but would ensure that no visible
seams were present where two faces meet _without_ requiring any "manual" normal
calculation. However, this would also require texture coordinates to be shared,
which could cause problems if we wanted textures to be tiled properly.

The "multiple geometries" approach.
-----------------------------------

Option 2 avoids the "space sharing" problem, but requires more memory. However,
"extra" geometry objects (and their instances) could be freed when not required
to offset this somewhat.

Per-vertex normals could be generated automatically, though with a caveat: face
edges would not share vertices with adjacent faces, causing visible seams.

Given that this approach would only require vertex positions to be updated (not
their associated indices), it should also be more-performant.

Notes
-----

It's theoretically possible that a very small sphere would need to render up to
five sides of the "cube" at once i.e. everything except the "opposite" face.

Option 1 could become even more complicated here, whereas Option 2 would "scale
up" fairly easily (though, again, at the cost of requiring even more memory).

That said: This problem could be avoided by simply drawing the **whole** sphere
if it's below a certain radius.
