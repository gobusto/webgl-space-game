class Graphics {
  constructor (selector, width, height) {
    this.renderer = new THREE.WebGLRenderer()
    this.renderer.setSize(width, height)
    this.renderer.setPixelRatio(window.devicePixelRatio)

    const target = document.querySelector(selector)
    target.appendChild(this.renderer.domElement)

    this.camera = new THREE.PerspectiveCamera(60, width/height, 0.1, 10000)
    this.scene = new THREE.Scene()

    this.sphereGeometry = new THREE.SphereGeometry(1, 32, 32)
    this.defaultMaterial = new THREE.MeshBasicMaterial({ color: '#0088FF' })

    this.actorMaterial = new THREE.MeshBasicMaterial({ color: '#FFFFFF' })
    this.actorGeometry = new THREE.ConeGeometry()
    this.actorGeometry.rotateX(Math.PI/2)

    // The below is all for debugging purposes...

    this.upMat = new THREE.LineBasicMaterial({ color: 0xffff00 })
    this.upLine = new THREE.BufferGeometry().setFromPoints(
      [new THREE.Vector3(0, 0, 0), new THREE.Vector3(0, 0, 100)]
    )

    this.leftMat = new THREE.LineBasicMaterial({ color: 0x00ffff })
    this.leftLine = new THREE.BufferGeometry().setFromPoints(
      [new THREE.Vector3(0, 0, 0), new THREE.Vector3(0, 0, 100)]
    )

    this.forwardMat = new THREE.LineBasicMaterial({ color: 0xff00ff })
    this.forwardLine = new THREE.BufferGeometry().setFromPoints(
      [new THREE.Vector3(0, 0, 0), new THREE.Vector3(0, 0, 100)]
    )
  }

  update (universe) {
    universe.getPlanets().forEach(planet => {
      this.drawObject(planet, this.sphereGeometry, this.defaultMaterial)
    })

    universe.getActors().forEach(actor => {
      this.drawObject(actor, this.actorGeometry, this.actorMaterial)

      actor._gfx.up.set(actor.up[0], actor.up[1], actor.up[2])
      actor._gfx.lookAt(
        actor.position[0] + actor.forward[0],
        actor.position[1] + actor.forward[1],
        actor.position[2] + actor.forward[2]
      )

      // Position the camera where the player is.
      this.camera.position.x = actor.position[0]
      this.camera.position.y = actor.position[1]
      this.camera.position.z = 300

      // The below is just for debugging purposes:

      if (!actor._lines) {
        actor._lines = [
          new THREE.Line(this.upLine, this.upMat),
          new THREE.Line(this.leftLine, this.leftMat),
          new THREE.Line(this.forwardLine, this.forwardMat)
        ]
        actor._lines.forEach(l => this.scene.add(l))
      }

      actor._lines.forEach(l => {
        l.position.set(actor.position[0], actor.position[1], actor.position[2])
      })

      actor._lines[0].lookAt(
        actor.position[0] + actor.up[0],
        actor.position[1] + actor.up[1],
        actor.position[2] + actor.up[2]
      )
      actor._lines[1].lookAt(
        actor.position[0] + actor.left[0],
        actor.position[1] + actor.left[1],
        actor.position[2] + actor.left[2]
      )
      actor._lines[2].lookAt(
        actor.position[0] + actor.forward[0],
        actor.position[1] + actor.forward[1],
        actor.position[2] + actor.forward[2]
      )

    })

    this.renderer.render(this.scene, this.camera)
  }

  drawObject(obj, geometry, material) {
    if (!obj._gfx) {
      obj._gfx = new THREE.Mesh(geometry, material)
      obj._gfx.scale.set(obj.radius, obj.radius, obj.radius)
      this.scene.add(obj._gfx)
    }

    obj._gfx.position.set(obj.position[0], obj.position[1], obj.position[2])
  }
}
