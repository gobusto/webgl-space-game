function crossProduct (foo, bar) {
  return [
      foo[1] * bar[2] - foo[2] * bar[1],
    -(foo[0] * bar[2] - foo[2] * bar[0]),
      foo[0] * bar[1] - foo[1] * bar[0]
  ]
}

function vectorLength (vec) {
  return Math.sqrt(vec[0]*vec[0] + vec[1]*vec[1] + vec[2]*vec[2])
}

function normalize (vec) {
  const t = vectorLength(vec)
  return t ? vec.map(v => v/t) : [0, 0, 0]
}

function directionFrom (foo, bar) {
  const a = foo.position[0] - bar.position[0]
  const b = foo.position[1] - bar.position[1]
  const c = foo.position[2] - bar.position[2]
  return normalize([a, b, c])
}

// Determine if two spheres overlap.
function overlap (foo, bar) {
  const a = foo.position[0] - bar.position[0]
  const b = foo.position[1] - bar.position[1]
  const c = foo.position[2] - bar.position[2]
  const d = foo.radius + bar.radius
  return a*a + b*b + c*c <= d*d
}

class Universe {
  constructor () {
    this.planets = [
      { position: [200, 150, 0], radius: 90, atmosphere: 30 },
      { position: [450, 300, 0], radius: 100, atmosphere: 30 },
      { position: [750, 350, 0], radius: 80, atmosphere: 50 }
    ]

    this.actors = [
      {
        kind: 'person',
        position: [300, 150, 0],
        radius: 10,
        velocity: [0, 0, 0],
        up: [1, 0, 0],
        left: [0, 1, 0],
        forward: [0, 0, 1]
      }
    ]
  }

  update (userInput) {
    this.actors.forEach(actor => {
      // Which planet are we on? May be undefined!
      const planet = this.planets.find(p =>
        overlap(actor, { position: p.position, radius: p.radius+p.atmosphere })
      )

      // HACK: Change a person into a ship or vice versa:
      if (!this.fireHeld && userInput.fire[1] && planet) {
        actor.kind = actor.kind === 'person' ? 'ship' : 'person'
      }
      this.fireHeld = userInput.fire[1]
      document.title = actor.kind

      // Most actor types will "inherit" their up-ness from the nearest planet:
      const localUp = planet ? directionFrom(actor, planet) : actor.up
      if (planet && actor.kind !== 'ship') { actor.up = localUp }

      if (actor.kind === 'person') {
        actor.velocity[0] -= 0.3 // Gravity.
        if (userInput.fire[0]) { actor.velocity[0] += 0.4 }

        actor.velocity[1] += userInput.axis[0] * 2
        actor.velocity[2] -= userInput.axis[1] * 2

        actor.velocity[1] *= 0.5
        actor.velocity[2] *= 0.5
      } else if (actor.kind === 'ship') {
        actor.velocity[2] += userInput.fire[0] ? 0.5 : 0

        // Ships "turn" by maniuplating the frame of reference i.e. their axes:
        const turn = userInput.axis[0] * 0.03
        const look = userInput.axis[1] * -0.03

        actor.forward = actor.forward.map(
          (value, axis) => value + actor.left[axis]*turn + actor.up[axis]*look
        )
        actor.up = crossProduct(actor.left, actor.forward)

        actor.velocity[0] *= 0.9
        actor.velocity[1] *= 0.9
        actor.velocity[2] *= 0.9
      }

      actor.left = normalize(crossProduct(actor.forward, actor.up))
      actor.forward = crossProduct(actor.up, actor.left)

      actor.position[0] += actor.velocity[0] * actor.up[0]
      actor.position[1] += actor.velocity[0] * actor.up[1]
      actor.position[2] += actor.velocity[0] * actor.up[2]

      actor.position[0] += actor.velocity[1] * actor.left[0]
      actor.position[1] += actor.velocity[1] * actor.left[1]
      actor.position[2] += actor.velocity[1] * actor.left[2]

      actor.position[0] += actor.velocity[2] * actor.forward[0]
      actor.position[1] += actor.velocity[2] * actor.forward[1]
      actor.position[2] += actor.velocity[2] * actor.forward[2]

      // Are we touching the planet surface?
      if (planet && overlap(actor, planet)) {
        // Position the actor EXACTLY on the surface of the planet:
        const d = actor.radius + planet.radius
        actor.position[0] = planet.position[0] + d * localUp[0]
        actor.position[1] = planet.position[1] + d * localUp[1]
        actor.position[2] = planet.position[2] + d * localUp[2]
        actor.velocity[0] = 0.3
      }
    })
  }

  getPlanets () {
    return this.planets
  }

  getActors () {
    return this.actors
  }
}
