class UserInput {
  constructor () {
    this.keys = {}
    document.body.onkeydown = this._keyboardEvent.bind(this)
    document.body.onkeyup = this._keyboardEvent.bind(this)
  }

  getState () {
    let result = { axis: [0, 0], fire: [false, false] }

    const joypad = navigator.getGamepads()[0]
    if (joypad) {
      if (Math.abs(joypad.axes[0]) > 0.3) { result.axis[0] = joypad.axes[0] }
      if (Math.abs(joypad.axes[1]) > 0.3) { result.axis[1] = joypad.axes[1] }
      result.fire[0] = joypad.buttons[0].pressed
      result.fire[1] = joypad.buttons[1].pressed
    }

    if (this.keys['a']) { result.axis[0] = -1 }
    if (this.keys['d']) { result.axis[0] = 1 }
    if (this.keys['w']) { result.axis[1] = -1 }
    if (this.keys['s']) { result.axis[1] = 1 }
    if (this.keys[' ']) { result.fire[0] = true }
    if (this.keys['enter']) { result.fire[1] = true }

    return result
  }

  _keyboardEvent (event) {
    const down = event.type === 'keydown'
    if (!event.repeat) { this.keys[event.key.toLowerCase()] = down }
  }
}
