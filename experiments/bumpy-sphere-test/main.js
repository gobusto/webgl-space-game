"use strict"

// These variables need to persist between each `requestAnimationFrame()` call:
let renderer, scene, camera

// Convert a geographic coordinate into a (sine-wave-modulated) XYZ coordinate.
function xyzFromGeoCoords (latitude, longitude, radius, waves) {
  const sinLatitude = Math.sin(latitude)
  const cosLatitude = Math.cos(latitude)
  const sinLongitude = Math.sin(longitude)
  const cosLongitude = Math.cos(longitude)

  // Convert the list of waves into an array of specific values for this point:
  const values = waves.map(wave => {
    const value = wave.useLatitude ? latitude : longitude
    const s = cosLatitude // Set this to 1.0 for "pure" results...
    return Math.sin((value + wave.offset) * wave.frequency) * wave.amplitude * s
  })

  // Add the values (and the radius!) together to determine the "combined" XYZ:
  const xyz = [0, 0, 0]
  values.concat([radius]).forEach(value => {
    xyz[0] += cosLatitude * value * cosLongitude
    xyz[1] += cosLatitude * value * sinLongitude
    xyz[2] += sinLatitude * value
  })
  return xyz
}

// Generate a spheroid mesh for a given radius (and set of "modulation" waves):
function calculateSpheroidGeometry (radius, xSamples, ySamples, waves) {
  const verts = []
  const indices = []

  for (let y = 0; y <= ySamples; ++y) {
    for (let x = 0; x <= xSamples; ++x) {
      const latitude = (y/ySamples) * Math.PI - (Math.PI/2)
      const longitude = (x/xSamples) * (Math.PI*2) - Math.PI
      const p = xyzFromGeoCoords(latitude, longitude, radius, waves)
      verts.push(p[0], p[1], p[2])

      if (x > 0 && y > 0) {
        indices.push(
          ((y - 1) * (xSamples+1)) + (x - 1),
          ((y - 1) * (xSamples+1)) + (x - 0),
          ((y - 0) * (xSamples+1)) + (x - 1),
          ((y - 0) * (xSamples+1)) + (x - 0),
          ((y - 0) * (xSamples+1)) + (x - 1),
          ((y - 1) * (xSamples+1)) + (x - 0)
        )
      }
    }
  }

  const geometry = new THREE.BufferGeometry()
  geometry.setIndex(indices)
  geometry.setAttribute('position', new THREE.Float32BufferAttribute(verts, 3))
  geometry.computeVertexNormals()
  return geometry
}

function setupScene (width, height) {
  renderer = new THREE.WebGLRenderer()
  renderer.setSize(width, height)
  renderer.setPixelRatio(window.devicePixelRatio)
  renderer.setClearColor("#14275e")
  document.body.appendChild(renderer.domElement)
  camera = new THREE.PerspectiveCamera(60, width/height, 0.01, 100)
  scene = new THREE.Scene()

  const waves = [
    { offset: 1, frequency: 3, amplitude: 0.06 },
    { offset: 1, frequency: 5, amplitude: 0.05 },
    { offset: 0, frequency: 7, amplitude: 0.02 },
    { offset: 2, frequency: 13, amplitude: 0.03, useLatitude: true },
    { offset: 2, frequency: 9, amplitude: 0.04, useLatitude: true },
    { offset: 0, frequency: 5, amplitude: 0.05, useLatitude: true }
  ]

  const geometry = calculateSpheroidGeometry(3, 100, 200, waves)
  const material = new THREE.MeshPhongMaterial({ color: "#f36267" })
  scene.add(new THREE.Mesh(geometry, material))

  // const points = new THREE.PointsMaterial({ color: "#ffffff", size: 0.05 })
  // scene.add(new THREE.Points(geometry, points))

  const sphereMaterial = new THREE.MeshPhongMaterial({ color: "#53bcb8" })
  const sphereGeometry = new THREE.SphereGeometry(3, 100, 200)
  const sphereInstance = new THREE.Mesh(sphereGeometry, sphereMaterial)
  scene.add(sphereInstance)

  const sun = new THREE.DirectionalLight(0xC0C0C0)
  sun.position.set(5, 5, 5)
  scene.add(sun)
  scene.add(new THREE.AmbientLight(0x505050))
}

function updateDisplay (timer) {
  requestAnimationFrame(updateDisplay)
  if (!timer) { return setupScene(640, 480) }

  camera.up = new THREE.Vector3(0, 0, 1)
  camera.position.set(
    Math.sin(timer/3000) * 7,
    Math.cos(timer/3000) * 7,
    5
  )
  camera.lookAt(0, 0, 0)

  renderer.render(scene, camera)
}

window.onload = function () { updateDisplay() }
