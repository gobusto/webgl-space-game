Bumpy Sphere Test
=================

This is a demonstration of how 2D "landscapes" can be generated for a planet by
modulating a basic sphere shape with sine waves. This same concept is also used
in music -- see <https://en.wikipedia.org/wiki/Additive_synthesis> -- though in
this case we are interested in the visual shape generated, rather than the tone
we would be hearing.

The basic idea is to use the latitude/longitude values as an "input" for one or
more sine waves (each with an independent offset, frequency, and amplitude), in
much the same way as "time" is used for musical contexts.

Advantages
----------

* A given "sine wave set" always generates the same results (even if they're in
  a different order) making this a useful technique for procedural generation.
* Additional sine waves add more "detail" to the result, so we get multiple LOD
  options "for free" if we choose to only process the `x` "loudest" ones.
* No precalculation required; the height for a given point can be calculated on
  the fly, without any knowledge of surrounding points.

Potential Problems
------------------

* Because the generated landscape is essentially a 2D grid projected on to a 3D
  sphere, data points are "denser" around the north/south poles; some method of
  biasing things towards the "equator" may be necessary to make the result look
  more uniform.
* A Perlin Noise approach would be able to generate "caves" via the use of a 3D
  noise function instead of a 2D one. This sine-based approach could work in 3D
  as well (by using distance-from-centre as a third input), but would the caves
  look good enough...?
* The results are arguably less "natural-looking" than Perlin/Simplex Noise is.

Notes:
------

A different algorithm for generating planets is described by Paul Bourke:

<http://paulbourke.net/fractals/noise/>
