// Get the canvas we want to draw to:
var canvas = document.querySelector("canvas")
// Ask it for a "context" to allow 2D drawing:
var context = canvas.getContext("2d")

function redraw (time) {
  // Schedule the next redraw:
  requestAnimationFrame(redraw)

  // Draw a background:
  context.fillStyle = "blue"
  context.fillRect(0, 0, canvas.width, canvas.height)

  // Draw a shape:
  var x = time / 10
  context.fillStyle = "red"
  context.fillRect(x, 50, 100, 100)
}

// Schedule the first redraw:
requestAnimationFrame(redraw)
