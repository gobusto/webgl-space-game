// Get the canvas we want to draw to:
var canvas = document.querySelector("canvas")
// Ask it for a "context" to allow 2D drawing:
var context = canvas.getContext("2d")

// Track which buttons are currently pressed:
var keyboard = {}
function keyChange (e) { keyboard[e.key.toUpperCase()] = e.type == 'keydown' }
document.body.onkeydown = keyChange
document.body.onkeyup = keyChange

// Track the current player position:
var x = 50

function redraw (time) {
  // Schedule the next redraw:
  requestAnimationFrame(redraw)

  // Change the player position based on which keys are currently pressed:
  if (keyboard['A']) { x = x - 2 }
  if (keyboard['D']) { x = x + 2 }

  // Draw a background:
  context.fillStyle = "blue"
  context.fillRect(0, 0, canvas.width, canvas.height)

  // Draw an image:
  var image = document.getElementById("theimage")
  context.drawImage(image, x, 50)
}

// Schedule the first redraw:
requestAnimationFrame(redraw)
