// This serves basically the same purpose as the canvas/context we had before:
var renderer
// This is where we store our 3D scene, before we ask the renderer to draw it:
var scene

// This is our "eye" position in 3D space:
var camera
// This is the "object" we want to control:
var object

// This is where we prepare things before trying to draw anything:
function setup (width, height) {
  scene = new THREE.Scene()
  camera = new THREE.PerspectiveCamera(60, width/height, 1, 1000)

  // Create a WebGL-enabled <canvas> and add it to the page:
  renderer = new THREE.WebGLRenderer()
  renderer.setSize(width, height)
  document.body.appendChild(renderer.domElement)

  // Set up some basic lighting:
  const sun = new THREE.DirectionalLight("white")
  sun.position.set(5, 5, 5)
  scene.add(sun)
  // Don't make things TOTALLY black if they are facing away from the light:
  scene.add(new THREE.AmbientLight("#555555"))

  // Create a 3D object:
  var geometry = new THREE.BoxGeometry()
  var material = new THREE.MeshPhongMaterial({ color: "red" })
  object = new THREE.Mesh(geometry, material)
  scene.add(object)
}

function redraw (time) {
  // Schedule the next redraw:
  requestAnimationFrame(redraw)

  object.position.set(0, 0, -2)
  // Reset the current rotation...
  object.setRotationFromMatrix(new THREE.Matrix4())
  // ...and then turn the object ? radians:
  object.rotateY(time / 1000)
  // Finally, re-render the scene so that we can see the changes:
  renderer.render(scene, camera)
}

// Set things up and then schedule the first redraw:
setup(640, 480)
requestAnimationFrame(redraw)

// Allow the user to make things run full-screen:
document.getElementById("fullscreen-button").onclick = function () {
  renderer.domElement.requestFullscreen()
}
