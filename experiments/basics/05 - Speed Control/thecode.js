// Get the canvas we want to draw to:
var canvas = document.querySelector("canvas")
// Ask it for a "context" to allow 2D drawing:
var context = canvas.getContext("2d")

// Track the current player position:
var x = 50

var lastTime = 0
function redraw (thisTime) {
  // Schedule the next redraw...
  requestAnimationFrame(redraw)
  // ...but don't bother continuing with THIS redraw if it happened too early:
  if (thisTime < lastTime + 16) { return }
  // If we ARE going to continue with this redraw, remember when it happened:
  lastTime = thisTime

  // Change the player position based on the left analogue stick:
  var joypads = navigator.getGamepads()
  if (joypads[0]) {
    if (joypads[0].axes[0] < -0.2) { x = x - 2 }
    if (joypads[0].axes[0] >  0.2) { x = x + 2 }
  }

  // Draw a background:
  context.fillStyle = "blue"
  context.fillRect(0, 0, canvas.width, canvas.height)

  // Draw an image:
  var image = document.getElementById("theimage")
  context.drawImage(image, x, 50)
}

// Schedule the first redraw:
requestAnimationFrame(redraw)

// Allow the user to make things run full-screen:
document.getElementById("fullscreen-button").onclick = function () {
  canvas.requestFullscreen()
}
