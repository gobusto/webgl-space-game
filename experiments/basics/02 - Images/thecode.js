// Get the canvas we want to draw to:
var canvas = document.querySelector("canvas")
// Ask it for a "context" to allow 2D drawing:
var context = canvas.getContext("2d")

function redraw (time) {
  // Schedule the next redraw:
  requestAnimationFrame(redraw)

  // Draw a background:
  context.fillStyle = "blue"
  context.fillRect(0, 0, canvas.width, canvas.height)

  // Draw an image:
  var x = time / 10
  var image = document.getElementById("theimage")
  context.drawImage(image, x, 50)
}

// Schedule the first redraw:
requestAnimationFrame(redraw)
