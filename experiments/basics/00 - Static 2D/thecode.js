// Get the canvas we want to draw to:
var canvas = document.querySelector("canvas")
// Ask it for a "context" to allow 2D drawing:
var context = canvas.getContext("2d")

// Draw something:
context.fillStyle = "red"
context.fillRect(20, 50, 100, 100)
