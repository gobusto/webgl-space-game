"use strict"

let renderer, camera, scene

function setupScene () {
  renderer = new THREE.WebGLRenderer()
  renderer.setSize(640, 480)
  renderer.setPixelRatio(window.devicePixelRatio)
  document.body.appendChild(renderer.domElement)

  camera = new THREE.PerspectiveCamera(60, 640/480, 0.01, 100)
  camera.position.set(0, 0, 25)

  scene = new THREE.Scene()

  const directionalLight = new THREE.DirectionalLight(0xffffff, 1.0)
  scene.add(directionalLight)
  directionalLight.position.set(5, 5, 5)
}

// We could use a canvas for this, but this is easier:
function createTexture () {
  const img = document.createElement('img')
  img.src = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAIAAAACCAYAAABytg0kAAAAFklEQVQIW2P8z8DwnxFIMAJpIGBkAAA8/wb+YGYULgAAAABJRU5ErkJggg=='

  const texture = new THREE.Texture(img)
  texture.wrapS = texture.wrapT = THREE.RepeatWrapping
  texture.minFilter = texture.magFilter = THREE.NearestFilter
  texture.needsUpdate = true
  return texture
}

// The number of vertices on a given "row" depends on the Y position:
function xForY (y, ySegments) {
  return 2 + (y < ySegments / 2 ? y : ySegments - y)
}

function createSphere (radius, ySegments) {
  if (!radius || radius <= 0) { radius = 1 }
  if (!ySegments || ySegments < 2) { ySegments = 2 }

  let uv = []
  let verts = []
  let indices = []

  let startOfRow = 0
  for (let y = 0; y <= ySegments; ++y) {
    const xThisRow = xForY(y + 0, ySegments)
    const xNextRow = xForY(y + 1, ySegments)

    for (let x = 0; x < xThisRow; ++x) {
      const lat = Math.PI * (y / ySegments)
      const lng = Math.PI * (x / xThisRow) * 2

      verts.push(
        radius * Math.cos(lng) * Math.sin(lat),
        radius * Math.cos(lat),
        radius * Math.sin(lng) * Math.sin(lat)
      )
      uv.push(x % 2, y % 2)

      if (x < xThisRow && x < xNextRow && y < ySegments) {
        const thisRowThisCol = x
        const nextRowThisCol = x % xNextRow
        const thisRowNextCol = (x + 1) % xThisRow
        const nextRowNextCol = (x + 1) % xNextRow

        const thisRow = startOfRow
        const nextRow = thisRow + xThisRow

        indices.push(
          thisRow + thisRowThisCol,
          thisRow + thisRowNextCol,
          nextRow + nextRowThisCol,
          thisRow + thisRowNextCol,
          nextRow + nextRowNextCol,
          nextRow + nextRowThisCol
        )
      }
    }

    // TODO: Insert extra triangles here...

    startOfRow += xThisRow
  }

  const geometry = new THREE.BufferGeometry()
  geometry.setIndex(indices)
  geometry.setAttribute('position', new THREE.Float32BufferAttribute(verts, 3))
  geometry.setAttribute('uv', new THREE.Float32BufferAttribute(uv, 2))
  geometry.computeVertexNormals()
  return geometry
}

function updateDisplay (timer) {
  requestAnimationFrame(updateDisplay)

  window.planet.setRotationFromAxisAngle(new THREE.Vector3(0, 1, 0), timer / 2000)

  renderer.render(scene, camera)
}

window.onload = function () {
  setupScene()

  const texture = createTexture()
  const geometry = createSphere(10, 20)
  const material = new THREE.MeshPhongMaterial({ map: texture })
  window.planet = new THREE.Mesh(geometry, material)
  scene.add(window.planet)

  updateDisplay()
}
