Modified UV Sphere Test
=======================

A UV Sphere is easy to render, but has an uneven vertex distribution; its poles
have a lot of vertices close together, whilst the equator spreads them out over
a larger radius. On the plus side, it is very easy to draw a "subset" of one by
specifying a start/end "row" and start/end "column", and generating texture U/V
coordinates for tiling textures requires no real effort.

Concept
-------

Rather than use the same number of vertices on each "row", we can insert one or
more "extra" vertices per "row" as we move towards the equator. This results in
a higher vertex density near the equator and a lowered number near the poles.

Each additional vertex requires an equal number of additional polygons to cover
the "gap" they create. In order to minimise warping of each "tile" -- to ensure
that their texture remains reasonably "unstretched" -- these should be inserted
halfway along the "row" i.e. opposite the starting "seam" line.

If we wanted to add 2 vertices per "row", we would add these at the 1/3 and 2/3
points since -- again -- this minimises the "stretching" of each "tile" so that
textures look reasonable. 3 vertices per "row" would insert the new polygons at
the 1/4, 2/4, and 3/4 points, and so forth.

Drawbacks
---------

Whilst the placement of additional triangles will reduce the "warping" for each
"square", the triangles themselves may look odd, as they will not tile properly
with their neighbouring "squares" unless a special (triangular) texture is used
for them.

Scaling the number of vertices _linearly_ might be an imperfect solution, since
they are "bent" around a curved surface. Having the number of extra vertices be
variable (based on latitude) would solve this, at the cost of extra complexity.

Finally, drawing a subset of this mesh is significantly more difficult than for
a basic UV Sphere, due to the variation in vertex/polygon count for any subset.

Alternatives
------------

The common alternative to a UV Sphere is a subdivided icosohedron, which avoids
the usual vertex density problems. However, it's not as easy to assign "square"
texture coordinates (though non-square tiling textures could work).

A better solution would be to use a normalised cube (or even a spherified one):

* <https://iquilezles.org/articles/patchedsphere/>
* <https://medium.com/@oscarsc/four-ways-to-create-a-mesh-for-a-sphere-d7956b825db4>

These result in a less-perfect vertex density, but are more naturally suited to
tiled (square) textures.
