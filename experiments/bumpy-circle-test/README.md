Bumpy Circle Test
=================

This is a demonstration of how 1D "landscapes" can be generated for a planet by
modulating a basic circle shape with sine waves. This same concept is also used
in music -- see <https://en.wikipedia.org/wiki/Additive_synthesis> -- though in
this case we are interested in the visual shape generated, rather than the tone
we would be hearing.

The basic idea is to use the "angle" (0 to 360 degrees) as an "input" value for
one or more sine waves (each of which has an independent offset, frequency, and
amplitude), in much the same way as "time" is used for musical contexts. Adding
more sine waves adds more "detail" to the result, so we get multiple LOD (level
of detail) options "for free" if we sort them by amplitude and only process the
first `x` of them i.e. the `x` "loudest" ones.

A given "sine wave set" always generates the same results -- even if they're in
a different order -- so this is a useful technique for procedural generation.
