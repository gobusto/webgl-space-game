// Get the height of our (2D) "planet" at a specific angle:
function calculateHeightAtPoint(a, radius, waves) {
  const sina = Math.sin(a)
  const cosa = Math.cos(a)

  // This defines the basic "circle" shape:
  let x = cosa * radius
  let y = sina * radius

  // Each wave then modifies that basic circle shape:
  waves.forEach(wave => {
    const i = Math.sin((a + wave.offset) * wave.frequency) * wave.amplitude
    x += cosa * i
    y += sina * i
  })

  return [x, y]
}

window.onload = function () {
  const segs = 512 // More "circle" segments == Smoother, more-accurate result.

  // These values define the "terrain" of our 2D "planet" via modulation:
  const waves = [
    { offset: 0, frequency: 1, amplitude: 0.04 },
    { offset: 1, frequency: 7, amplitude: 0.03 },
    { offset: 1, frequency: 21, amplitude: 0.02 },
    { offset: 2, frequency: 37, amplitude: 0.01 },
  ]

  // Set up a square canvas:
  const canvas = document.createElement('canvas')
  canvas.width = 600
  canvas.height = 600
  document.body.appendChild(canvas)

  // Set up a -1...+1 coordinate system for both axes:
  const ctx = canvas.getContext('2d')
  ctx.translate(600/2, 600/2)
  ctx.scale(600/3, 600/3)

  // Draw the "planet" shape:
  ctx.beginPath()
  for (let s = 0 ; s <= segs; ++s) {
    const a = (s/segs) * 2*Math.PI
    p = calculateHeightAtPoint(a, 1.3, waves)
    s === 0 ? ctx.moveTo(p[0], p[1]) : ctx.lineTo(p[0], p[1])
  }
  ctx.fill()
}
