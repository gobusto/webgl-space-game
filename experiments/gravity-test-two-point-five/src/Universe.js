// Determine if two circles overlap.
function overlap (foo, bar) {
  const a = foo.position[0] - bar.position[0]
  const b = foo.position[1] - bar.position[1]
  const d = foo.radius + bar.radius
  return a*a + b*b <= d*d
}

class Universe {
  constructor () {
    this.planets = [
      { position: [200, 150, 0], radius: 90, atmosphere: 30 },
      { position: [450, 300, 0], radius: 100, atmosphere: 30 },
      { position: [750, 350, 0], radius: 80, atmosphere: 50 }
    ]

    this.actors = [
      { kind: 'person', position: [250, 150, 0], radius: 10, velocity: [0, 0, 0], angle: 0 }
    ]
  }

  update (userInput) {
    this.actors.forEach(actor => {
      // Which planet are we on? May be undefined!
      const planet = this.planets.find(p =>
        overlap(actor, { position: p.position, radius: p.radius+p.atmosphere })
      )

      // HACK: Change a person into a ship or vice versa:
      if (!this.fireHeld && userInput.fire[1] && planet) {
        actor.kind = actor.kind === 'person' ? 'ship' : 'person'
      }
      this.fireHeld = userInput.fire[1]

      // Normal user input:
      if (actor.kind === 'person') {
        // Movement and Gravity (relative to local planet "up" direction):
        actor.velocity[0] -= 0.3
        if (userInput.fire[0]) { actor.velocity[0] += 0.4 }
        actor.velocity[1] -= userInput.axis[0] * 2
        actor.velocity[1] *= 0.5

        // If we're on a planet, update the "up" angle for the actor:
        if (planet) {
          const a = actor.position[0] - planet.position[0]
          const b = actor.position[1] - planet.position[1]
          actor.angle = Math.atan2(b, a)
        }
      } else if (actor.kind === 'ship') {
        // Movement is not relative to anything; use global coordinates.
        actor.angle -= userInput.axis[0] * 0.03
        if (userInput.fire[0]) { actor.velocity[0] += 0.7 }
        actor.velocity[0] *= 0.9
        actor.velocity[1] *= 0.9
      }

      actor.position[0] += actor.velocity[0] * Math.cos(actor.angle)
      actor.position[0] -= actor.velocity[1] * Math.sin(actor.angle)
      actor.position[1] += actor.velocity[0] * Math.sin(actor.angle)
      actor.position[1] += actor.velocity[1] * Math.cos(actor.angle)

      // Are we touching the planet surface?
      if (planet && overlap(actor, planet)) {
        const a = actor.position[0] - planet.position[0]
        const b = actor.position[1] - planet.position[1]
        const planetAngle = Math.atan2(b, a)
        if (actor.kind === 'person') { actor.angle = planetAngle }

        // Position the actor EXACTLY on the surface of the planet:
        const d = actor.radius + planet.radius
        actor.position[0] = planet.position[0] + d * Math.cos(planetAngle)
        actor.position[1] = planet.position[1] + d * Math.sin(planetAngle)
        actor.velocity[0] = 0
      }
    })
  }

  getPlanets () {
    return this.planets
  }

  getActors () {
    return this.actors
  }
}
