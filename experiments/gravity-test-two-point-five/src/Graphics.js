class Graphics {
  constructor (selector, width, height) {
    this.renderer = new THREE.WebGLRenderer()
    this.renderer.setSize(width, height)
    this.renderer.setPixelRatio(window.devicePixelRatio)

    const target = document.querySelector(selector)
    target.appendChild(this.renderer.domElement)

    this.camera = new THREE.PerspectiveCamera(60, width/height, 0.1, 10000)
    this.scene = new THREE.Scene()

    this.sphereGeometry = new THREE.SphereGeometry(1, 32, 32)
    this.defaultMaterial = new THREE.MeshBasicMaterial({ color: '#0088FF' })

    this.actorGeometry = new THREE.ConeGeometry()
    this.actorGeometry.rotateZ(Math.PI / -2)

    this.actorMaterial = new THREE.MeshBasicMaterial({ color: '#FFFFFF' })
  }

  update (universe) {
    this.camera.position.x = 500
    this.camera.position.y = 300
    this.camera.position.z = 500

    universe.getPlanets().forEach(planet => {
      this.drawObject(planet, this.sphereGeometry, this.defaultMaterial)
    })

    universe.getActors().forEach(actor => {
      this.drawObject(actor, this.actorGeometry, this.actorMaterial)
      actor._gfx.setRotationFromMatrix(new THREE.Matrix4())
      actor._gfx.rotateZ(actor.angle)
    })

    this.renderer.render(this.scene, this.camera)
  }

  drawObject(object, geometry, material) {
    if (!object._gfx) {
      object._gfx = new THREE.Mesh(geometry, material)
      object._gfx.scale.set(object.radius, object.radius, object.radius)
      this.scene.add(object._gfx)
    }

    object._gfx.position.set(object.position[0], object.position[1], object.position[2])
  }
}
