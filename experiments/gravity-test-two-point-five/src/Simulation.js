class Simulation {
  constructor (selector) {
    this.userInput = new UserInput()
    this.universe = new Universe()
    this.graphics = new Graphics(selector, 800, 450)
  }

  mainLoop (currentTime) {
    requestAnimationFrame(this.mainLoop.bind(this))
    if (this.lastUpdate && this.lastUpdate > currentTime - 16) { return }
    this.lastUpdate = currentTime

    this.universe.update(this.userInput.getState())
    this.graphics.update(this.universe)
  }
}
