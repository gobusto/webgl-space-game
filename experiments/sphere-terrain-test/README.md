Sphere Terrain Test
===================

This is a demonstration of how a partial sphere can be rendered for cases where
we might (for example) only want to draw nearby terrain whilst walking around a
planet.

Conceptually, we use a "grid of tiles" which can be "dropped on to" the surface
of the sphere, akin to draping a cloth over a ball.

Advantages
----------

* Terrain rendering works identically to a "standard" scenario, since the tiles
  are still a regular grid -- just "bent around" a sphere.
* The "draping the cloth" step works if the sphere is "bumpy" -- useful when we
  want to have a planet with terrain, rather than a perfect sphere.
* Stretching the "cloth" over a wider area means that we can render more of the
  sphere at a lower level-of-detail -- useful if a viewer can move closer to or
  further away from the surface of a planet.
* The grid isn't "directly" tied to any specific position on the surface of the
  sphere, so we don't run into "vertex density" problems for poles vs. equator.

Disadvantages
-------------

* It can be difficult to quantise the grid so that moving from one "tile" to an
  adjacent one does not cause a noticeable "snap" -- especially once we texture
  the tiles.
* Because the "angle" of the grid is not fixed, texture position/orientation is
  not consistent; by trying to keep the tile grid aligned to a local viewer (so
  that moving from one tile to the next is not obvious), there is a side effect
  that the tiles will be mapped differently when the viewer approaches the same
  position from a different "entry angle" later on.

Notes
-----

Possible alternative solutions:

## UV Sphere

Drawing a partial "UV sphere" is easy to do, and different levels of detail can
easily be achieved by increasing/decreasing the segment count for each axis.

Unfortunately, this kind of sphere doesn't have a uniform vertex density; tiles
near poles will be much smaller than ones near the equator, causing problems in
terms of consistent texture sizes (from a visual POV) and vertex/polygon counts
(from a performance POV).

## Icosahedron

A subdivided icosahedron largely avoids vertex density issues, and can be drawn
at varying levels of detail.

Tiling textures should be possible, but probably not traditional "square" ones.
