const xSamples = 12
const ySamples = 8

let renderer, camera, scene, geometry

// Euclidean -> Lat/Long. See https://www.oc.nps.edu/oc2902w/coord/coordcvt.pdf
function geoCoordsFrom (origin, point) {
  const x = point[0] - origin[0]
  const y = point[1] - origin[1]
  const z = point[2] - origin[2]
  const p = Math.sqrt(x*x + y*y)

  return { longitude: Math.atan2(y, x), latitude: Math.atan2(z, p) }
}

// Take an XYZ coordinate, and "bend it" around a sphere.
function xyzFrom (sphere, point) {
  // Convert an XYZ point into local latitude/longitude values...
  const coords = geoCoordsFrom (sphere.position, point)

  // .. and then try to "re-map" them back into XYZ coordinates:
  return [
    sphere.position[0] + Math.cos(coords.latitude) * sphere.radius * Math.cos(coords.longitude),
    sphere.position[1] + Math.cos(coords.latitude) * sphere.radius * Math.sin(coords.longitude),
    sphere.position[2] + Math.sin(coords.latitude) * sphere.radius
  ]
}

function setupScene (sphere) {
  renderer = new THREE.WebGLRenderer()
  renderer.setSize(640, 480)
  renderer.setPixelRatio(window.devicePixelRatio)
  document.body.appendChild(renderer.domElement)

  camera = new THREE.PerspectiveCamera(60, 640/480, 0.01, 100)
  scene = new THREE.Scene()

  let verts = []
  let indices = []

  for (let y = 0; y < ySamples; ++y) {
    for (let x = 0; x < xSamples; ++x) {
      verts.push(0, 0, 0) // Not calculated yet...

      if (x > 0 && y > 0) {
        indices.push(
          ((y - 1) * xSamples) + (x - 1),
          ((y - 1) * xSamples) + (x - 0),
          ((y - 0) * xSamples) + (x - 1),
          ((y - 0) * xSamples) + (x - 0),
          ((y - 0) * xSamples) + (x - 1),
          ((y - 1) * xSamples) + (x - 0)
        )
      }
    }
  }

  const material = new THREE.MeshBasicMaterial({ color: "#aaeeff" })
  geometry = new THREE.BufferGeometry()
  geometry.setIndex(indices)
  geometry.setAttribute('position', new THREE.Float32BufferAttribute(verts, 3))
  scene.add(new THREE.Mesh(geometry, material))

  const sphereMaterial = new THREE.MeshBasicMaterial({ color: "#aa0000" })
  sphereGeometry = new THREE.SphereGeometry(sphere.radius * 0.99, 32, 32)
  sphereInstance = new THREE.Mesh(sphereGeometry, sphereMaterial)
  scene.add(sphereInstance)
  sphereInstance.position.set(
    sphere.position[0],
    sphere.position[1],
    sphere.position[2]
  )

  camera.position.set(5, 5, 20)
}

function updateDisplay (timer) {
  const sphere = { position: [5, 5, -10], radius: 10 }

  requestAnimationFrame(updateDisplay)
  if (!timer) { return setupScene(sphere) }

  for (let y = 0; y < ySamples; ++y) {
    for (let x = 0; x < xSamples; ++x) {
      const xyz = xyzFrom(sphere, [x + Math.sin(timer/1000) * 10, y + 4, 0])
      const i = (y * xSamples) + x

      geometry.attributes.position.array[i*3 + 0] = xyz[0]
      geometry.attributes.position.array[i*3 + 1] = xyz[1]
      geometry.attributes.position.array[i*3 + 2] = xyz[2]
    }
  }
  geometry.attributes.position.needsUpdate = true
  geometry.computeBoundingSphere()

  renderer.render(scene, camera)
}

window.onload = function () { updateDisplay() }
