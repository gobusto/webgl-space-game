Experiments and Prototypes
==========================

This directory contains various prototypes for ideas before they are (possibly)
integrated into the main game engine. The intention is for them to act as basic
proof-of-concept demonstrations in order to ascertain whether or not they would
work in practice -- though they also make useful stand-alone "examples" for the
features they are intended to implement.
